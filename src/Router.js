import React, {useContext, useEffect} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {createStackNavigator} from '@react-navigation/stack';

import LoginForm from './layouts/auth/Login';
import Home from './layouts/auth/Home';
import Registration from './layouts/auth/Registration';
import {AppContext} from './context/AppContext';
import Dashboard from './layouts/Dashboard';
import Loading from './resource/Loading';

const AuthStack = createStackNavigator();

const Router = () => {
  const {user, loading} = useContext(AppContext);
  useEffect(() => {
    if (loading.apps_loaded) {
      SplashScreen.hide();
    }
  }, [loading.apps_loaded]);
  return (
    <>
      {loading.apps_loaded ? (
        <AuthStack.Navigator>
          {user.is_signed_in ? (
            <>
              <AuthStack.Screen
                options={{headerShown: false}}
                name="Dashboard"
                component={Dashboard}
              />
            </>
          ) : (
            <>
              <AuthStack.Screen
                options={{headerShown: false}}
                name="Home"
                component={Home}
              />
              <AuthStack.Screen
                options={{headerShown: false}}
                name="Login"
                component={LoginForm}
              />
              <AuthStack.Screen
                options={{headerShown: false}}
                name="Registration"
                component={Registration}
              />
            </>
          )}
        </AuthStack.Navigator>
      ) : (
        <Loading />
      )}
    </>
  );
};
export default Router;
