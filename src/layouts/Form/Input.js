import React from 'react'
import { TextInput } from 'react-native';

const Input = ({value,onChangeText,secureTextEntry,placeholder}) => {
 return   <TextInput
        value={value}
        onChangeText={onChangeText}
        underlineColorAndroid="transparent"
        secureTextEntry={secureTextEntry||false}
        placeholder={placeholder||''}
        autoCapitalize="none" />
}

export default Input;

