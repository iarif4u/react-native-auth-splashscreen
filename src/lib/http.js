import Axios from "axios";
const http={
    getData:(url,token)=>Axios.get(url,{
        headers: {
            Authorization: "Bearer "+token
        }}),
    postData:(url,data)=>Axios.post(url,data),
    postDataToken:(url,data,token)=>Axios.post(url,data,{
        headers: {
            Authorization: "Bearer "+token
        }}),
    patchData:(url,data,token)=>Axios.patch(url,data, {
        headers: {
            Authorization: "Bearer "+token
        }}),
    deleteData:(url,token)=>Axios.delete(url, {
        headers: {
            Authorization: "Bearer "+token
        }}),
}

export default http;
