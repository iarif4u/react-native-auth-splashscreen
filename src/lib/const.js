import {Dimensions} from 'react-native';
const APP_URL = 'https://mirsarai.xyz/wecarry/wp-json';
const LOGIN_URL = `${APP_URL}/jwt-auth/v1/token`;
const APPS_COLOR = 'red';
const LOGO = require('./../assets/logo.jpg');
const PROFILE = require('./../assets/profile.png');
const TOP_LOGO = require('./../assets/BellBoyLogo.jpg');
const DISPLAY_WIDTH = Dimensions.get('window').width;
const DISPLAY_HEIGHT = Dimensions.get('window').height;
const BANNER = require('./../assets/banner.png');
const ROW_WIDTH = Math.floor(DISPLAY_WIDTH - DISPLAY_WIDTH / 15);
const TRANS_LOGO = require('./../assets/bellboy.png');

export {
  APPS_COLOR,
  DISPLAY_HEIGHT,
  LOGO,
  PROFILE,
  TOP_LOGO,
  ROW_WIDTH,
  TRANS_LOGO,
  BANNER,
  LOGIN_URL,
};
