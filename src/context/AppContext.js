import React, {createContext, useReducer, useEffect} from 'react';
import {showMessage, hideMessage} from 'react-native-flash-message';

import LoadingReducer from './LadingReducer';
import {IS_STARTUP, LOADING, USER_SIGN_IN} from './ReducerConst';
import UserReducer from './UserReducer';

import http from '../lib/http';
import {LOGIN_URL} from '../lib/const';
import {getJson, setJson} from '../lib/keystore';

const initialState = {
  products: [],
  user: {
    name: '',
    email: '',
    phone: '',
    password: '',
    is_signed_in: false,
  },
  loading: {
    is_loading: false,
    apps_loaded: false,
  },
};

export const AppContext = createContext(initialState);
export const AppProvider = ({children}) => {
  const [loadingState, dispatchLoading] = useReducer(
    LoadingReducer,
    initialState.loading,
  );
  const [userState, dispatchUser] = useReducer(UserReducer, initialState.user);

  const makeLoading = (status) => {
    dispatchLoading({
      type: LOADING,
      payload: status,
    });
  };
  const changeUserData = (type, data) => {
    dispatchUser({
      type: type,
      payload: data,
    });
  };

  const submitLogin = () => {
    makeLoading(true);
    http
      .postData(LOGIN_URL, {
        username: userState.email,
        password: userState.password,
      })
      .then((response) => {
        showMessage({
          message: 'Login Successfully done',
          type: 'success',
        });
        setJson('token', response.data.token).then(() => {
          dispatchUser({
            type: USER_SIGN_IN,
            payload: true,
          });
        });
      })
      .catch((e) => {
        showMessage({
          message: 'Invalid username or password',
          type: 'danger',
        });
      })
      .then(() => makeLoading(false));
  };
  useEffect(() => {
    getJson('token')
      .then((token) => {
        if (token) {
          dispatchUser({
            type: USER_SIGN_IN,
            payload: true,
          });
        }
      })
      .then(() => {
        dispatchLoading({
          type: IS_STARTUP,
          payload: true,
        });
      });
  }, []);
  return (
    <AppContext.Provider
      value={{
        loading: loadingState,
        user: userState,
        makeLoading,
        changeUserData,
        submitLogin,
      }}>
      {children}
    </AppContext.Provider>
  );
};
